package com.aurora.gplayapi;

import com.aurora.gplayapi.exceptions.AuthException;
import com.aurora.gplayapi.modals.ApiBuilder;
import com.aurora.gplayapi.providers.HeaderProvider;
import com.aurora.gplayapi.providers.ParamProvider;
import com.aurora.gplayapi.retro.RetroClient;
import com.aurora.gplayapi.retro.RetroService;
import com.aurora.gplayapi.utils.Util;
import okhttp3.*;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class GooglePlayApi {

    private static final String URL_AUTH = "https://android.clients.google.com/auth";
    private static final String URL_CHECK_IN = "https://android.clients.google.com/checkin";
    private static final String URL_FDFE = "https://android.clients.google.com/fdfe";
    private static final String URL_TOC = URL_FDFE + "/toc";
    private static final String URL_TOS_ACCEPT = URL_FDFE + "/acceptTos";
    private static final String URL_UPLOAD_DEVICE_CONFIG = URL_FDFE + "/uploadDeviceConfig";

    private ApiBuilder builder;

    public GooglePlayApi(ApiBuilder builder) {
        this.builder = builder;
    }

    public TocResponse toc() throws IOException {
        final RetroService retroService = RetroClient.getInstance().create(RetroService.class);
        final Call<ResponseBody> call = retroService.get(URL_TOC, HeaderProvider.getDefaultHeaders(builder));
        final Response<ResponseBody> response = call.execute();

        final byte[] responseBytes = response.body().bytes();

        final TocResponse tocResponse = ResponseWrapper.parseFrom(responseBytes).getPayload().getTocResponse();

        if (tocResponse.hasTosContent() && tocResponse.hasTosToken()) {
            AcceptTosResponse acceptTosResponse = acceptTos(tocResponse.getTosToken());
        }

        if (tocResponse.hasCookie()) {
            builder.setDfeCookie(tocResponse.getCookie());
        }

        return tocResponse;
    }

    private AcceptTosResponse acceptTos(String tosToken) throws IOException {
        final Map<String, String> headers = HeaderProvider.getDefaultHeaders(builder);
        final Map<String, String> params = new HashMap<>();
        params.put("tost", tosToken);
        params.put("toscme", "false");

        final Response<ResponseBody> response = getAuthResponse(URL_TOS_ACCEPT, headers, params);
        final byte[] responseBytes = response.body().bytes();
        return ResponseWrapper.parseFrom(responseBytes).getPayload().getAcceptTosResponse();
    }

    public UploadDeviceConfigResponse uploadDeviceConfig() throws IOException {
        final UploadDeviceConfigRequest request = UploadDeviceConfigRequest.newBuilder()
                .setDeviceConfiguration(builder.getDeviceInfoProvider().getDeviceConfigurationProto())
                .build();
        final Map<String, String> headers = HeaderProvider.getDefaultHeaders(builder);
        headers.put("X-DFE-Enabled-Experiments", "cl:billing.select_add_instrument_by_default");
        headers.put("X-DFE-Unsupported-Experiments", "nocache:billing.use_charging_poller,market_emails,buyer_currency,prod_baseline,checkin.set_asset_paid_app_field,shekel_test,content_ratings,buyer_currency_in_app,nocache:encrypted_apk,recent_changes");
        headers.put("X-DFE-SmallestScreenWidthDp", "320");
        headers.put("X-DFE-Filter-Level", "3");

        final Request.Builder requestBuilder = new Request.Builder()
                .url(URL_UPLOAD_DEVICE_CONFIG)
                .post(RequestBody.create(MediaType.parse("application/x-protobuf"), request.toByteArray()));

        final RetroService retroService = RetroClient.getInstance().create(RetroService.class);
        final Call<ResponseBody> call = retroService.post(URL_UPLOAD_DEVICE_CONFIG, headers, requestBuilder.build().body());
        final Response<ResponseBody> response = call.execute();

        final UploadDeviceConfigResponse deviceConfigResponse = ResponseWrapper.parseFrom(response.body().bytes())
                .getPayload()
                .getUploadDeviceConfigResponse();

        if (deviceConfigResponse.hasUploadDeviceConfigToken()) {
            builder.setDeviceConfigToken(deviceConfigResponse.getUploadDeviceConfigToken());
        }
        return deviceConfigResponse;
    }

    public String generateGsfId() throws IOException {
        final AndroidCheckinRequest request = builder.getDeviceInfoProvider().generateAndroidCheckInRequest();
        final AndroidCheckinResponse checkInResponse = checkIn(request.toByteArray());
        final String gsfId = BigInteger.valueOf(checkInResponse.getAndroidId()).toString(16);

        builder.setGsfId(gsfId);
        builder.setDeviceCheckInConsistencyToken(checkInResponse.getDeviceCheckinConsistencyToken());

        return gsfId;
    }

    private AndroidCheckinResponse checkIn(byte[] request) throws IOException {
        Map<String, String> headers = HeaderProvider.getAuthHeaders(builder);
        headers.put("Content-Type", "application/x-protobuffer");
        headers.put("Host", "android.clients.google.com");

        final Request.Builder requestBuilder = new Request.Builder()
                .url(URL_CHECK_IN)
                .post(RequestBody.create(MediaType.parse("application/x-protobuf"), request));
        final RetroService retroService = RetroClient.getInstance().create(RetroService.class);
        final Call<ResponseBody> call = retroService.post(URL_CHECK_IN, headers, requestBuilder.build().body());
        final Response<ResponseBody> response = call.execute();
        final byte[] content = response.body().bytes();

        return AndroidCheckinResponse.parseFrom(content);
    }

    public String generateAASToken(String email, String oauthToken) throws IOException {
        final Map<String, String> params = new HashMap<>();
        params.putAll(ParamProvider.getDefaultLoginParams(builder));
        params.putAll(HeaderProvider.getAASTokenHeaders(email, oauthToken));

        final Map<String, String> headers = HeaderProvider.getAuthHeaders(builder);
        headers.put("app", "com.android.vending");

        final Response<ResponseBody> response = getAuthResponse(URL_AUTH, headers, params);

        if (response.body() == null) {
            final String err = response.errorBody() != null ? response.errorBody().string() : "Unknown server side issue";
            throw new AuthException(err);
        }

        final Map<String, String> hashMap = Util.parseResponse(response.body().string());
        if (hashMap.containsKey("Token")) {
            return hashMap.get("Token");
        } else {
            throw new AuthException("Authentication failed");
        }
    }

    public String generateToken(String aasToken) throws IOException {
        final Map<String, String> params = new HashMap<>();
        params.putAll(ParamProvider.getDefaultLoginParams(builder));
        params.putAll(ParamProvider.getTokenParams(aasToken));

        final Map<String, String> headers = HeaderProvider.getAuthHeaders(builder);
        headers.put("app", "com.google.android.gms");

        final Response<ResponseBody> response = getAuthResponse(URL_AUTH, headers, params);

        if (response.body() == null) {
            final String err = response.errorBody() != null ? response.errorBody().string() : "Unknown server side issue";
            throw new AuthException(err);
        }

        final Map<String, String> hashMap = Util.parseResponse(response.body().string());
        if (hashMap.containsKey("Auth")) {
            String token = hashMap.get("Auth");
            builder.setAuthToken(token);
            return token;
        } else {
            throw new AuthException("Authentication failed");
        }
    }

    private Response<ResponseBody> getAuthResponse(String URL, Map<String, String> headers, Map<String, String> params) throws IOException {
        final FormBody.Builder bodyBuilder = new FormBody.Builder();

        for (String name : params.keySet()) {
            bodyBuilder.add(name, params.get(name));
        }

        final RetroService retroService = RetroClient.getInstance().create(RetroService.class);
        final Call<ResponseBody> call = retroService.post(URL, headers, bodyBuilder.build());
        final Response<ResponseBody> response = call.execute();
        return response;
    }
}
