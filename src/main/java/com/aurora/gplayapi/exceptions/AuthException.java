package com.aurora.gplayapi.exceptions;

import lombok.Data;

import java.io.IOException;

@Data
public class AuthException extends IOException {

    protected int code;
    protected byte[] rawResponse;

    public AuthException(String message) {
        super(message);
    }

    public AuthException(String message, int code) {
        super(message);
        this.code = code;
    }

    public AuthException(String message, Throwable cause) {
        super(message);
        initCause(cause);
    }
}