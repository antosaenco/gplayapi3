package com.aurora.gplayapi.exceptions;

import lombok.Data;

import java.io.IOException;

@Data
public class GooglePlayException extends IOException {

    protected int code;
    protected byte[] rawResponse;

    public GooglePlayException(String message) {
        super(message);
    }

    public GooglePlayException(String message, int code) {
        super(message);
        this.code = code;
    }

    public GooglePlayException(String message, Throwable cause) {
        super(message);
        initCause(cause);
    }
}