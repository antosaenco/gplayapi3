package com.aurora.gplayapi.retro;

import retrofit2.Retrofit;
import retrofit2.converter.protobuf.ProtoConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetroClient {

    private static final String BASE_URL = "https://android.clients.google.com/";
    private static Retrofit retrofit;

    public static Retrofit getInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(ProtoConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
