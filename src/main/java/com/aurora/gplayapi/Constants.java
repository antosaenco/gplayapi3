package com.aurora.gplayapi;

public class Constants {

    public enum ABUSE {
        SEXUAL_CONTENT(1),
        GRAPHIC_VIOLENCE(3),
        HATEFUL_OR_ABUSIVE_CONTENT(4),
        IMPROPER_CONTENT_RATING(5),
        HARMFUL_TO_DEVICE_OR_DATA(7),
        OTHER(8),
        ILLEGAL_PRESCRIPTION(11),
        IMPERSONATION(12);

        public int value;

        ABUSE(int value) {
            this.value = value;
        }
    }

    public enum PATCH_FORMAT {
        GDIFF(1),
        GZIPPED_GDIFF(2),
        GZIPPED_BSDIFF(3),
        UNKNOWN_4(4),
        UNKNOWN_5(5);

        public int value;

        PATCH_FORMAT(int value) {
            this.value = value;
        }
    }

    public enum REVIEW_SORT {
        NEWEST(0), HIGHRATING(1), HELPFUL(4);

        public int value;

        REVIEW_SORT(int value) {
            this.value = value;
        }
    }

    public enum RECOMMENDATION_TYPE {
        ALSO_VIEWED(1), ALSO_INSTALLED(2);

        public int value;

        RECOMMENDATION_TYPE(int value) {
            this.value = value;
        }
    }

    public enum SEARCH_SUGGESTION_TYPE {
        SEARCH_STRING(2), APP(3);

        public int value;

        SEARCH_SUGGESTION_TYPE(int value) {
            this.value = value;
        }
    }

    public enum SUBCATEGORY {
        TOP_FREE("apps_topselling_free"),
        TOP_GROSSING("apps_topgrossing"),
        MOVERS_SHAKERS("apps_movers_shakers");

        public String value;

        SUBCATEGORY(String value) {
            this.value = value;
        }
    }

    public enum LIBRARY_ID {
        WISH_LIST("u-wl");
        public String value;

        LIBRARY_ID(String value) {
            this.value = value;
        }
    }

    public enum APP_STREAM_TAB {
        INSTALLED,
        UPDATES,
        LIBRARY
    }
}
